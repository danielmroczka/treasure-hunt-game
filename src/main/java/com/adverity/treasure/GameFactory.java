package com.adverity.treasure;

import com.adverity.treasure.functional.GameFunctionalImpl;
import com.adverity.treasure.oop.GameObjectOrientedImpl;

/**
 * Instantiates class implementation depends on user selection.
 * Triggers exception once data is unknown.
 */
public class GameFactory {
    public static Game create(GameImplementation name, int[][] array) {
        switch (name) {
            case OOP:
                return new GameObjectOrientedImpl(array);
            case FUNCTIONAL:
                return new GameFunctionalImpl(array);
            default:
                throw new IllegalStateException("Unexpected value: " + name);
        }
    }

    public enum GameImplementation {FUNCTIONAL, OOP}
}
