package com.adverity.treasure;

import java.util.Set;

/**
 * Common interface to be implemented by OOP or Functional solution
 */
public interface Game {

    /**
     * Triggers execution of game
     */
    void start();

    /**
     * Common report code for all implementations
     *
     * @param found
     * @param visited
     */
    default void report(boolean found, Set<Integer> visited) {
        if (found) {
            visited.forEach(i -> System.out.print(String.format("%d %d", i / 10, i % 10) + "\n"));
        } else {
            System.out.println("NO TREASURE");
        }
    }

}
