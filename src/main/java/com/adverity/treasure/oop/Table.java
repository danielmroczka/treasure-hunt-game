package com.adverity.treasure.oop;

class Table {
    private final int[][] tab;

    public Table(int[][] tab) {
        this.tab = tab;
    }

    /**
     * Each cell contains a number where the ten’s digit represents the row number
     * and the unit’s digit represents the column number of the cell containing the next clue.
     *
     * @param decUnitPosition
     * @return
     */
    public int getValue(int decUnitPosition) {
        int col = decUnitPosition / 10;
        int row = decUnitPosition % 10;

        return getValue(row, col);
    }

    private int getValue(int row, int col) {
        return tab[col - 1][row - 1];
    }

}
