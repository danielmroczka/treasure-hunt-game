package com.adverity.treasure.oop;

import com.adverity.treasure.Game;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Implementation of Treasure Hunt Game in OOP approach (recursive)
 */
public class GameObjectOrientedImpl implements Game {

    /**
     * Tracks already visited cells to avoid infinite loops
     */
    private final Set<Integer> visited = new LinkedHashSet<>();
    private final Table table;
    private boolean found;

    public GameObjectOrientedImpl(int[][] array) {
        this.table = new Table(array);
    }

    @Override
    public void start() {
        /* Set start position to 11 (first row, first column) at the beginning of game. */
        move(11);
        report(found, visited);
    }

    /**
     * Move to next cell as long as you haven't found treasure or infinite loop has been detected
     *
     * @param pos next cell position
     */
    private void move(int pos) {
        if (!found && visited.add(pos)) {
            int nextPos = table.getValue(pos);
            found = nextPos == pos;
            move(nextPos);
        }
    }
}
