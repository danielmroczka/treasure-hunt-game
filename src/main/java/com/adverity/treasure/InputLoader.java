package com.adverity.treasure;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.adverity.treasure.GameFactory.GameImplementation.FUNCTIONAL;
import static com.adverity.treasure.GameFactory.GameImplementation.OOP;

/**
 * Class responsibility is to read and validate input data provided by user from STDIN.
 * Once provided data has been validated it executes selected implementation of Treasue Hunt Game.
 */
class InputLoader {

    /**
     * Regex pattern expression accepts only two digits. Each digit must be have value from range 1 to 5.
     */
    private final static String DATA_FORMAT = "([0-5]{2}\\s+){4}([0-5]{2}\\s*)";
    private static final int SIZE = 5;
    private final int[][] inputArray;

    InputLoader() {
        inputArray = new int[SIZE][SIZE];
        execute();
    }

    /**
     * Method reads input data and accepts only value matches pro
     */
    private void execute() {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("*** Treasure Hunt Game ***");
            System.out.println("Enter the treasure array (5x5) element by element");
            System.out.println("Please provide data in format: dd dd dd dd dd then please press enter");
            System.out.println("Enter empty line to exit\n");
            for (int row = 0; row < SIZE; row++) {
                enterSingleLine(input, row);
            }
        }

        System.out.println("\nResults from Object Oriented Programming Solution:");
        GameFactory.create(OOP, inputArray).start();
        System.out.println("\nResults from Functional approach Solution:");
        GameFactory.create(FUNCTIONAL, inputArray).start();
    }

    /**
     * Method responsible for read one number from STDIN.
     * Validates data and repeat prompt once data is not valid.
     *
     * @param input
     * @param row
     * @param col
     */
    private void enterSingleLine(Scanner input, int row) {
        String line = null;
        do {
            System.out.printf("Enter values for row (%d): ", row);

            if (input.hasNextLine()) {
                String text = input.nextLine();
                if (text.isEmpty()) {
                    System.exit(0);
                } else if (Pattern.matches(DATA_FORMAT, text)) {
                    line = text;
                } else {
                    System.err.println("Wrong format data! Please enter five numbers followed by space in format [0-5][0-5] [0-5][0-5] [0-5][0-5] [0-5][0-5] [0-5][0-5]");
                }
            }

        } while (line == null);

        inputArray[row] = Stream.of(line.split("\\s+")).mapToInt(Integer::parseInt).toArray();
    }

}
