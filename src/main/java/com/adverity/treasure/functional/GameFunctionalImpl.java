package com.adverity.treasure.functional;

import com.adverity.treasure.Game;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * Implementation of Treasure Hunt Game in Functional approach (not recursive)
 */
public class GameFunctionalImpl implements Game {

    /**
     * Tracks already visited cells to avoid infinite loops
     */
    private final Set<Integer> visited = new LinkedHashSet<>();

    private final int[][] array;

    public GameFunctionalImpl(int[][] array) {
        this.array = array;
    }

    private Function<Integer, Integer> next() {
        return arg -> {
            int col = arg / 10;
            int row = arg % 10;
            return array[col - 1][row - 1];
        };
    }

    @Override
    public void start() {
        Function<Integer, Integer> x = next();
        /* Set start position to 11 (first row, first column) at the beginning of game. */
        int moveToPos = 11;
        boolean found = false;

        while (!found && visited.add(moveToPos)) {
            int tmp = x.apply(moveToPos);
            found = tmp == moveToPos;
            moveToPos = tmp;
        }

        report(found, visited);
    }
}
