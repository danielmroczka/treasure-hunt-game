package com.adverity.treasure.oop;

import com.adverity.treasure.Examples;
import com.adverity.treasure.Game;
import com.adverity.treasure.GameFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class GameObjectOrientedImplTest {

    private final ByteArrayOutputStream testOut = new ByteArrayOutputStream();

    @Before
    public void before() {
        System.setOut(new PrintStream(testOut));
    }

    @Test
    public void shouldPassHappyPath() {
        //GIVEN
        Game game = GameFactory.create(GameFactory.GameImplementation.OOP, Examples.example01());
        //WHEN
        game.start();
        //THEN
        assertArrayEquals(Examples.expectedResult01().split("\n"), testOut.toString().split("\n"));
    }

    @Test
    public void shouldNotFoundTreasure() {
        //GIVEN
        Game game = GameFactory.create(GameFactory.GameImplementation.OOP, Examples.infinite01());
        //WHEN
        game.start();
        //THEN
        assertEquals("NO TREASURE", testOut.toString().trim());
    }

}