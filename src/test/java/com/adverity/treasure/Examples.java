package com.adverity.treasure;

public class Examples {
    public static int[][] example01() {
        return new int[][]{{55, 14, 25, 52, 21},
                {44, 31, 11, 53, 43},
                {24, 13, 45, 12, 34},
                {42, 22, 43, 32, 41},
                {51, 23, 33, 54, 15}};
    }

    /**
     * Results for example01 set
     *
     * @return
     */
    public static String expectedResult01() {
        return "1 1\n" +
                "5 5\n" +
                "1 5\n" +
                "2 1\n" +
                "4 4\n" +
                "3 2\n" +
                "1 3\n" +
                "2 5\n" +
                "4 3\n";
    }

    /**
     * Infinite path points always to next cell and never to itself.
     *
     * @return
     */
    public static int[][] infinite01() {
        return new int[][]{{12, 13, 14, 15, 21},
                {22, 23, 24, 25, 31},
                {32, 33, 34, 35, 41},
                {42, 43, 44, 45, 51},
                {52, 53, 54, 55, 11}};
    }
}
