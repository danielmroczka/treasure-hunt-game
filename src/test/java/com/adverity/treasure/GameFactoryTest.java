package com.adverity.treasure;

import com.adverity.treasure.functional.GameFunctionalImpl;
import com.adverity.treasure.oop.GameObjectOrientedImpl;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GameFactoryTest {

    @Test
    public void shouldCreateFunctionalGameInstance() {
        assertTrue(GameFactory.create(GameFactory.GameImplementation.FUNCTIONAL, null) instanceof GameFunctionalImpl);
    }

    @Test
    public void shouldCreateOOPGameInstance() {
        assertTrue(GameFactory.create(GameFactory.GameImplementation.OOP, null) instanceof GameObjectOrientedImpl);
    }

}